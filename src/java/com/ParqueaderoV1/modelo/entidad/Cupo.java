/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ParqueaderoV1.modelo.entidad;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "cupo")
@NamedQueries({
    @NamedQuery(name = "Cupo.findAll", query = "SELECT c FROM Cupo c")
    , @NamedQuery(name = "Cupo.findByIdCupo", query = "SELECT c FROM Cupo c WHERE c.idCupo = :idCupo")
    , @NamedQuery(name = "Cupo.findByTipo", query = "SELECT c FROM Cupo c WHERE c.tipo = :tipo")
    , @NamedQuery(name = "Cupo.findByEstado", query = "SELECT c FROM Cupo c WHERE c.estado = :estado")
    , @NamedQuery(name = "Cupo.findByTarifa", query = "SELECT c FROM Cupo c WHERE c.tarifa = :tarifa")})
public class Cupo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idCupo")
    private Integer idCupo;
    @Column(name = "tipo")
    private Character tipo;
    @Column(name = "Estado")
    private Character estado;
    @Column(name = "Tarifa")
    private Integer tarifa;
    @OneToMany(mappedBy = "idCupo")
    private List<Parquedo> parquedoList;

    public Cupo() {
    }

    public Cupo(Integer idCupo) {
        this.idCupo = idCupo;
    }

    public Integer getIdCupo() {
        return idCupo;
    }

    public void setIdCupo(Integer idCupo) {
        this.idCupo = idCupo;
    }

    public Character getTipo() {
        return tipo;
    }

    public void setTipo(Character tipo) {
        this.tipo = tipo;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Integer getTarifa() {
        return tarifa;
    }

    public void setTarifa(Integer tarifa) {
        this.tarifa = tarifa;
    }

    public List<Parquedo> getParquedoList() {
        return parquedoList;
    }

    public void setParquedoList(List<Parquedo> parquedoList) {
        this.parquedoList = parquedoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCupo != null ? idCupo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cupo)) {
            return false;
        }
        Cupo other = (Cupo) object;
        if ((this.idCupo == null && other.idCupo != null) || (this.idCupo != null && !this.idCupo.equals(other.idCupo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ParqueaderoV1.modelo.entidad.Cupo[ idCupo=" + idCupo + " ]";
    }
    
}
