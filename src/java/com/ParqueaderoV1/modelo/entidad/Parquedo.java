/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ParqueaderoV1.modelo.entidad;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "parquedo")
@NamedQueries({
    @NamedQuery(name = "Parquedo.findAll", query = "SELECT p FROM Parquedo p")
    , @NamedQuery(name = "Parquedo.findById", query = "SELECT p FROM Parquedo p WHERE p.id = :id")
    , @NamedQuery(name = "Parquedo.findByEntrada", query = "SELECT p FROM Parquedo p WHERE p.entrada = :entrada")
    , @NamedQuery(name = "Parquedo.findBySalida", query = "SELECT p FROM Parquedo p WHERE p.salida = :salida")
    , @NamedQuery(name = "Parquedo.findByMinutos", query = "SELECT p FROM Parquedo p WHERE p.minutos = :minutos")
    , @NamedQuery(name = "Parquedo.findByValor", query = "SELECT p FROM Parquedo p WHERE p.valor = :valor")})
public class Parquedo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "entrada")
    @Temporal(TemporalType.TIMESTAMP)
    private Date entrada;
    @Column(name = "salida")
    @Temporal(TemporalType.TIMESTAMP)
    private Date salida;
    @Column(name = "minutos")
    private Integer minutos;
    @Column(name = "valor")
    private Long valor;
    @JoinColumn(name = "idVehiculo", referencedColumnName = "id")
    @ManyToOne
    private Vehiculo idVehiculo;
    @JoinColumn(name = "IdCupo", referencedColumnName = "idCupo")
    @ManyToOne
    private Cupo idCupo;

    public Parquedo() {
    }

    public Parquedo(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getEntrada() {
        return entrada;
    }

    public void setEntrada(Date entrada) {
        this.entrada = entrada;
    }

    public Date getSalida() {
        return salida;
    }

    public void setSalida(Date salida) {
        this.salida = salida;
    }

    public Integer getMinutos() {
        return minutos;
    }

    public void setMinutos(Integer minutos) {
        this.minutos = minutos;
    }

    public Long getValor() {
        return valor;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }

    public Vehiculo getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(Vehiculo idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public Cupo getIdCupo() {
        return idCupo;
    }

    public void setIdCupo(Cupo idCupo) {
        this.idCupo = idCupo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parquedo)) {
            return false;
        }
        Parquedo other = (Parquedo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ParqueaderoV1.modelo.entidad.Parquedo[ id=" + id + " ]";
    }
    
}
