/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ParqueaderoV1.modelo.facades;

import com.ParqueaderoV1.modelo.entidad.Parquedo;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author alejo
 */
@Stateless
public class ParquedoFacade extends AbstractFacade<Parquedo> implements ParquedoFacadeLocal {

    @PersistenceContext(unitName = "ParqueaderoV1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ParquedoFacade() {
        super(Parquedo.class);
    }
    
}
