/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ParqueaderoV1.modelo.facades;

import com.ParqueaderoV1.modelo.entidad.Cupo;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author alejo
 */
@Stateless
public class CupoFacade extends AbstractFacade<Cupo> implements CupoFacadeLocal {

    @PersistenceContext(unitName = "ParqueaderoV1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CupoFacade() {
        super(Cupo.class);
    }

    @Override
    public int contarEstadoCupo(String estado, String tipo) {

        TypedQuery<Cupo> query = (TypedQuery<Cupo>) getEntityManager().createNamedQuery("Cupo.findAll");
        List<Cupo> cupos = new ArrayList<>();

        cupos = query.getResultList();

        int cantidad = 0;
        for (int i = 0; i < cupos.size(); i++) {

            String e = cupos.get(i).getEstado().toString();
            String t = cupos.get(i).getTipo().toString();
            if (e.equals(estado)) {
                if (t.equals(tipo)) {
                    cantidad = cantidad + 1;
                } else {
                    cantidad = cantidad + 0;
                }
            } else {
                cantidad = cantidad + 0;
            }

        }
        return cantidad;

    }

}
