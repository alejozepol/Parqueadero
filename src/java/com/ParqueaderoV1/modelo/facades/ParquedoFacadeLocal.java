/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ParqueaderoV1.modelo.facades;

import com.ParqueaderoV1.modelo.entidad.Parquedo;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author alejo
 */
@Local
public interface ParquedoFacadeLocal {

    void create(Parquedo parquedo);

    void edit(Parquedo parquedo);

    void remove(Parquedo parquedo);

    Parquedo find(Object id);

    List<Parquedo> findAll();

    List<Parquedo> findRange(int[] range);

    int count();
    
}
